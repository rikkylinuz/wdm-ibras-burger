<!DOCTYPE html>
<html>
<head>
	<title>Edit Products</title>
	<link href="//db.onlinewebfonts.com/c/41f5e8ff1d98d490a19c6d48ea7b74b1?family=Beyond+The+Mountains" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo asset('css/ibras.css')?>">
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
</head>
<body id="wrapper" class ="rest" style="color:white;" >
	<header class ="rest">
		<div id = "header-gradeout">
			<img src="/images/5.png" class="logo" width="100px" align="center" />	
				<a href="/products">MANAGE PRODUCTS</a>
				<a class="active">EDIT PRODUCT</a>
		</div>

	</header>
	

	

	<h1 style="margin-top: 4rem;">Edit/Delete product</h1>
	<div class=form-container>
	    
		<form action ="/products/{{$product->id}}/update" method="post" id="newEntry" style="margin: auto; width: 14%;" enctype="multipart/form-data">

            @csrf
            @method('PATCH')
			<label for="bname">Name of the burger : </label>
			<input type="text" name="bname" id="bname" value="{{$product->burger_name}}"><br><br>

			<label for="bdesc">Description : </label>
			<input type="text" name="bdesc" id="bdesc" value="{{$product->description}}"><br><br>



			<label for="bprice">Price of the burger</label>
			<input type="text" name="bprice" id="bprice" value="{{$product->price}}" ><br><br>


			<label>Image:</label>
			<img src="/{{$product->image}}" style="max-height: 150px; max-width: 150px;"><br><br>



			<label for="imageToUpload" style="">Change image</label>
			<input type="file" name="imageToUpload" id="imageToUpload" >


             <input id ="AddProduct" type="submit" value="Edit Product" name="submitproduct" style="background-color: #008CBA;
    color: #fff;">
    
            



		</form>
       <p style="color: white;"> @error('bname') {{ "Check the Burger name field" }} @enderror </p>



       <form action="/products/{{$product->id}}/delete" method="post" style="margin-top: 5%; margin: auto; width: 14%;">
       	@method('DELETE')
       	@csrf

	       <button id ="DeleteProduct"  >Delete Product </button>

       </form>
		
		
	</div>
	
	
	

    <footer style="background:none;margin-top: 15rem;">
		<p>
				Copyright  &copy;2020 Todos los derechos reservados | Este sitio esta hecho con &hearts; por DiazApps
			</p>
	</footer>
</body>
</html>