
@section('content')

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<title>Contacto</title>
	<link href="//db.onlinewebfonts.com/c/41f5e8ff1d98d490a19c6d48ea7b74b1?family=Beyond+The+Mountains" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo asset('css/ibras.css')?>">

</head>
<body id="wrapper" class="rest">

	<div id ="modal" class="modal-register-gradeout">
		
		<div id ="modal-register">
			<span id = "closebtn" class="closebtn">&times;</span>
			<div id ="register-title">				
				<img id="burger-icon" src="<?php echo asset('images/Burguer.png')?>">
				Registro de Usario
			</div>
			<br><br>
			<hr>


			<form action="register" method="post" id="registration-form">
				{{ csrf_field() }}

				<p style="color: red;"> 

				@error('username') {{ $message }} @enderror 
				 @error('email') {{ $message }} @enderror
				 @error('email') {{ $message }} @enderror 
				 @error('password') {{ $message }} @enderror
				 @error('repeatpass') {{ $message }} @enderror
				  @error('address') {{ $message }} @enderror


				</p>

				<label for="fullname">Nombre y apellido:</label>
				<input type="text" name="username" id=fullname required
				    title="Username must have only alphabets and numbers."
				    pattern="^[a-zA-Z0-9]*$"
				    >

				<label for="mail">Correo:</label>
				<input type="email" name="email" id="mail" required
				    title="Example email: youremail@gmail.com"
				    pattern="[a-z0-9._%+-]+@gmail.com">
 

				<label for="pass">Contrasena:</label>
				<input type="password" name="password" id="pass" required
				    title="Password must contain at least 8 characters upto 10 characters, including atleast one uppercase, lowercase, number and special character." 
				    pattern='^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,10}$' 
				    >

				<label for="repeatPass">Repetir Contrasena:</label>
				<input type="password" name="repeatpass" id="repeatPass" required
				    title="Please enter the same Password as before."
				    pattern='^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,10}$' 
				    >
				<label for="address">Direccion:</label>
				<textarea name="address" id = "addresss" required></textarea>

				<input type="submit" name="submitregistration" value="Cargar" id  ="sendBtn">

			</form>
			
		</div>
	</div>

	<div id ="modal1" class="modal-login-gradeout">
		<div id ="modal-login">
			<span id = "closebtn1" class="closebtn">&times;</span>
			<div id ="login-title">				
				<img id="burger-icon" src="<?php echo asset('images/Burguer.png')?>">
				Iniciar Session
			</div>
			<br><br>
			<hr>


			<form action="login" method="post" id="login-form">
				{{ csrf_field() }}

				<p style="color: red;">
					@error('username') {{ $message }} @enderror
					@error('password') {{ $message }} @enderror 


				</p>

				<label for="userName">Usuario:</label>
				<input type="text" name="username" id=userName required>

				<label for="pass1">Contrasena:</label>
				<input type="password" name="password" id="pass1" required >

				<a href="#"><input type="submit" value="Entrar" id  ="enterBtn"></a>
				@if(Session::has('message'))
              	<p>
        	    	{{ Session::get('message') }}
               	</p>
           		@endif 
			</form>
		</div>
	</div>


	<header class="rest">
		<div id = "header-gradeout">
			<img src="<?php echo asset('images/5.png')?>" class="logo" width="100px" align="center" />
				@if (Session::has('user'))
				{
				<a  href="index.php">INICIO</a>
				<a href="sobrenostros">SOBRE NOSTROS</a>
				<a href="menu">MENU</a>
				<a href="blog/">BLOG</a>
				<a class="active" href="contacto">CONTACTO</a>
				<a  href="editarperfil">EDITAR PERFIL</a>
				<a  href="logout">CERRAR SESION</a> 
				}
				@else{
					<a href="index.php">INICIO</a>
					<a href="sobrenostros">SOBRE NOSTROS</a>
					<a href="menu">MENU</a>
					<a href="blog/">BLOG</a>
					<a class="active" href="contacto">CONTACTO</a>
					<a id ="registerBtn" >REGISTRO</a>
					<a id ="loginBtn" >INICIAR SESION</a>
				}
			    @endif
		</div>
		<div class="contactobanner">
			
		<div id ="banner-gradeout">
				<div class ="banner-text">
					<h3 class="intro">DI HOLA</h3>
					<h2 class='intro'>Contacto</h2>
				</div>
			</div>
		</div>	

	</header>

	<div class="contactobody">
		<br>
		 <!-- <img src="<?php echo asset('images/Burguer.png')?>" alt="burger-icon" width="50" height=50 align="top center"> --> 
		<h2 class="intro">Di Hola</h2>
		<span id="mediumfont">Di Hola, envianos un mensaje</span><br><br>
		Enivianos tus comentarios y suguerencias ustedes son importante para nosotros<br><br>
		<div class="container">    
			@if(Session::has('success'))
              	<div class="alert alert-success">
        	    	{{ Session::get('success') }}
               	</div>
           @endif        
		  <form method="post" action="contacto">
		  	{{csrf_field()}}
		  	<div class="row">
		  		<p style="color: red;"> @error('name') {{ $message }} @enderror </p>
		  		<p style="color: red;"> @error('email') {{ $message }} @enderror </p>
			  	<div class="col1">
			  		<input style="width: 100%; padding: .5rem;border: 1px solid black;" type="text" id="fname" name="name" placeholder="Name" 
			  		      title="Username must have only alphabets."
				          pattern="[a-zA-Z\s]+" />
			  	</div>	
		    	<div class="col2">
		    		<input style="width: 100%; padding: .5rem;border: 1px solid black;" type="text" id="email" name="email" placeholder="E-mail"  
		    		    title="Example email: youremail@gmail.com"
				       />	
		    	</div>
		    </div>
		    <p style="color: red;"> @error('subject') {{ $message }} @enderror </p>
		    	<textarea id="subject" name="subject" placeholder="Subject" 
		    	    title="Subject length should be less than 80 characters." maxlength="80"></textarea>

		    	   <p style="color: red;"> @error('message') {{ $message }} @enderror </p>
		    	<textarea id="message" name="message" placeholder="Message" style="height: 200px;" ></textarea>
			    <input type="submit" name ="submit" value="ENVIAR MENSAJE" id  ="messagebtn">
		  </form>
		    
		</div>
	</div>
	<footer>
		<div class="bgcolor">
			<img src="<?php echo asset('images/5.png')?>" class="logoFooter" align="center">
			<p>	
				<span id="title">Habla a:</span><br>
				Av. Intercomunal, sectro la Mora, calle 8
			</p>
			<p>
				<span id="title">Telefono:</span><br>
				+58 251 261 00 01
			</p>
			<p>
				<span id="title">Correo:</span><br>
				yourmail@gmail.com
			</p>
			<p>
				<a href="#" class="fa fa-pinterest"></a>
				<a href="#" class="fa fa-facebook"></a>
				<a href="#" class="fa fa-twitter"></a>
				<a href="#" class="fa fa-dribbble"></a>
				<a href="#" class="fa fa-google"></a>
				<a href="#" class="fa fa-linkedin"></a>
				<a href="#" class="fa fa-vimeo"></a>
			</p>
			<p>
				Copyright  &copy;2020 Todos los derechos reservados | Este sitio esta hecho con &hearts; por DiazApps
			</p>
		</div>
	</footer>
	<script type="text/javascript" src="<?php echo asset('js/main.js')?>"></script>
</body>
</html>