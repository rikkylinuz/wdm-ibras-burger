<!DOCTYPE html>
<html>
<head>
	<title>Products Page</title>
	<link href="//db.onlinewebfonts.com/c/41f5e8ff1d98d490a19c6d48ea7b74b1?family=Beyond+The+Mountains" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo asset('css/ibras.css')?>">
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
</head>
<body id="wrapper" class ="rest" style="color:white;" >
	<header class ="rest">
		<div id = "header-gradeout">
			<img src="images/5.png" class="logo" width="100px" align="center" />
				<a href="users">MANAGE USERS</a>
				<a class="active">MANAGE PRODUCTS</a>
				<a href="logout" >CERRAR SESION</a>
		</div>

	</header>
	

	<div id ="userctn" style="color:black;">
		<h1>List of products</h1>
		<table class ="user-list">
			<thead class = "user-table-head">
				<tr>
					<th>Burger name</th>
					<th>Description</th>
					<th>Price</th>
					<th>Images</th>
                    <th>Click to edit</th>
				</tr>
			</thead>
			<tbody class="user-table-body">

			@foreach($burgers as $burger)

                <tr>
                    <td>{{$burger->burger_name}}</td>
                    <td>{{$burger->description}}</td>
                    <td>{{$burger->price}}</td>
                    <td>{{$burger->image}}</td>
                    <td><a href="/products/{{$burger->id}}" style="color: red;">Edit</a></td>
                </tr>
                @endforeach
    			
				
			</tbody>


		</table>

	</div>
    
	<br><br><br><br><hr>

	<h1 style="margin-top: 4rem;">ADD A NEW MENU ITEM</h1>
	<div class=form-container>
	    
		<form action ="/burger/create" method="post" id="newEntry" style="margin: auto; width: 14%;" enctype="multipart/form-data">

            @csrf
			<label for="bname">Name of the burger : </label>
			<input type="text" name="bname" id="bname" ><br><br>

			<label for="bdesc">Description : </label>
			<input type="text" name="bdesc" id="bdesc" ><br><br>


			<label for="bprice">Price of the burger</label>
			<input type="text" name="bprice" id="bprice" ><br><br>

			<label for="imageToUpload" style="">Upload burger images :</label>
			<input type="file" name="imageToUpload" id="imageToUpload" >

             <input id ="AddProduct" type="submit" value="Add Product" name="submitproduct" style="background-color: #EF0031;
    color: #fff;">
            



		</form>
       <p style="color: white;"> @error('bname') {{ "Check the Burger name field" }} @enderror </p>
		
		
	</div>
	
	
	
    <footer style="background:none;margin-top: 8rem;">
		<p>
				Copyright  &copy;2020 Todos los derechos reservados | Este sitio esta hecho con &hearts; por DiazApps
			</p>
	</footer>
</body>
</html>