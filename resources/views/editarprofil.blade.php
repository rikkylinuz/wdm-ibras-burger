
<!DOCTYPE html>
<html>
<head>
	<title>Edit Profile</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo asset('css/ibras.css')?>">
</head>
<body id="wrapper" class="rest">
	<header class="rest">
		<div id = "header-gradeout">
			<img src="<?php echo asset('images/5.png')?>" class="logo" width="100px" align="center" />
				<a href="index.php">INICIO</a>
				<a href="sobrenostros">SOBRE NOSTROS</a>
				<a href="menu">MENU</a>
				<a href="blog/">BLOG</a>
				<a href="contacto">CONTACTO</a>
				<a class="active" href ="editarperfil">EDITAR PERFIL</a>
				<a id ="loginBtn" href="logout">CERRAR SESION</a>
		</div>
		<div class="menubanner">
			
		</div>	

	</header>
	    
		<div>
			<div class="editprofilecontainer">
		    <h1>Edit Profile</h1>
      		<div class="editprofilerow">
		      <div>
		        <h3>Personal Info</h3>
		        
				
				<form method="post" action="editarperfil/profile">
					@method('PATCH')
					@csrf

					<p style="color: red;">
					@error('firstname') {{ $message }} @enderror
					@error('lastname') {{ $message }} @enderror 
					@error('email') {{ $message }} @enderror 
					</p>


		          <div class="form-group">
		            <label>First name:</label>
		            <div>
		              <input class="inputprofile" type="text" name="firstname" value="{{ $user->firstname }}"
		                title="Name must have only alphabets."
		            </div>
		          </div>
		          <div class="form-group">
		            <label >Last name:</label>
		            <div>
		              <input class="inputprofile" type="text" name="lastname" value="{{ $user->lastname }}"
		                title="Name must have only alphabets."
		            </div>
		          </div>
		          <div class="form-group">
		            <label>Email:</label>
		            <div>
		              <input class="inputprofile" type="text" name="email" value="{{ $user->email }}"
		                title="Example email: youremail@gmail.com"
				        pattern="[a-z0-9._%+-]+@gmail.com">
		            </div>
		          </div>
		          <div class="form-group">
		            <label>Country:</label>
		            <div>
		              <input class="inputprofile" type="text" name="country" value="{{ $user->country }}"
		                title="Name must have only alphabets."
				        pattern="^[a-zA-Z\s]*$">
		            </div>
		          </div>
		          <div class="form-group">
		            <label>Mobile Number:</label>
		            <div>
		              <input class="inputprofile" type="text" name="mobile" value="{{ $user->mobile }}"
		                title="Mobile number must have only numbers of length 10."
				        pattern="^[0-9]{10}$">
		            </div>
		          </div>
		          <br>
		          <div class="form-group">
		            <label></label>
		            <div >
		              <input type="submit" name ="submit" class="btn" value="Save Changes">
		              <span></span>
		              <input type="reset" class="btn" value="Cancel">
		            </div>
		          </div>
		          	@if(Session::has('successProfile'))
		              	<div class="alert alert-success">
		        	    	{{ Session::get('successProfile') }}
		               	</div>
           			@endif  
		          <br>
		        </form>
		        </div>
		   </div>
		       <div class="editprofilerow">
		        <h3>Change Password</h3>
		       	<form method="post" action="editarperfil/password">
		       		@method('PATCH')
					@csrf
		        	<div class="form-group">
		            <label>Password:</label>
		            <div>
		              <input class="inputprofilepass" type="password" name="password" value="{{ $user->password }}"
		                title="Password must contain at least 8 characters upto 10 characters, including atleast one uppercase, lowercase, number and special character." 
				        pattern='^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,10}$'>
		            </div>
		          </div>
		          <div class="form-group">
		            <label>Confirm password:</label>
		            <div>
		              <input class="inputprofilepass" type="password" name="repeatpassword" value="{{ $user->password }}"
		                title="Please enter the same Password as before."
				        pattern='^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,10}$' >
		            </div><br>
		          </div>
		          <div class="form-group">
		            <label></label>
		            <div >
		              <input type="submit" name ="submitpass" class="btn" placeholder="Save Changes">
		              <span></span>
		              <input type="reset" class="btn" value="Cancel">
		            </div>
		          </div>
		          @if(Session::has('success'))
		              	<div class="alert alert-success">
		        	    	{{ Session::get('success') }}
		               	</div>
           			@endif 
		          <br>
		        </form>
		      </div>
		  </div>
		</div>

	<footer>
		<div class="bgcolor">
			<img src="<?php echo asset('images/5.png')?>" class="logoFooter" align="center">
			<p>	
				<span id="title">Habla a:</span><br>
				Av. Intercomunal, sectro la Mora, calle 8
			</p>
			<p>
				<span id="title">Telefono:</span><br>
				+58 251 261 00 01
			</p>
			<p>
				<span id="title">Correo:</span><br>
				yourmail@gmail.com
			</p>
			<p>
				<a href="#" class="fa fa-pinterest"></a>
				<a href="#" class="fa fa-facebook"></a>
				<a href="#" class="fa fa-twitter"></a>
				<a href="#" class="fa fa-dribbble"></a>
				<a href="#" class="fa fa-google"></a>
				<a href="#" class="fa fa-linkedin"></a>
				<a href="#" class="fa fa-vimeo"></a>
			</p>
			<p>
				Copyright  &copy;2020 Todos los derechos reservados | Este sitio esta hecho con &hearts; por DiazApps
			</p>
		</div>
	</footer>

</body>
</html>