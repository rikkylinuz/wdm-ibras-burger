<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <title>Cart</title>
        <link href="//db.onlinewebfonts.com/c/41f5e8ff1d98d490a19c6d48ea7b74b1?family=Beyond+The+Mountains" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   
        <link rel="stylesheet" type="text/css" href="/css/ibras.css">
    </head>
    <body id="wrapper" class="rest">
        <header class="rest" id="burgerheader">
        <img src="/images/5.png" class="logo" width="100px" align="center" />
            <a href="/menu">MENU</a>
            
            <a class = "active" href="/cart">CART
                
            </a>
            <div class="burgerbanner">
            <div class ="banner-text">
                    <h1 class="intro">Online Order System</h1>
                </div>
                
            </div>
        </header>
        <main>
        <div class="cart-wrapper">
            <h1>Purchase Cart</h1>
            <form action="updatecart" method="post">
                @csrf
                <table id="cartid" align="center";>
                    <thead>
                        <tr>
                            <td colspan="2">Product</td>
                            <td>Price</td>
                            <td>Quantity</td>
                            <td>Sub-Total</td>
                        </tr>
                    </thead>
                    <tbody>
                        @if (! Session :: has ('cart') || empty (Session :: get ('cart')))
                            <tr>
                                <td colspan="5" style="text-align:center;">You have no products added in your Shopping Cart</td>
                            </tr>
                        @else
                            <?php $subtotal=0?>
                            @foreach ($burgers as $burger)  
                            <?php $subtotal += $burger['price'] * $burger['quantity'] ?>
                            <tr>
                                <td class="img">
                                    <a href="">
                                        <img src="/{{$burger['photo']}}" width="50" height="50" alt="{{$burger['name']}}">
                                    </a>
                                </td>
                                <td>
                                    <a href="/burgerdetails/{{$burger['id']}}">{{$burger['name']}}</a>
                                    <br>
                                    <a href="/remove/{{$burger['id']}}" class="remove">Remove</a>
                                </td>
                                <td class="price">&dollar;{{$burger['price']}}</td>
                                <td class="quantity">
                                    <input type="number" name="quantity-{{$burger['id']}}" value="{{$burger['quantity']}}" min="1" max="15" p required>
                                </td>
                                <td class="price">&dollar;{{ $burger['price']  *  $burger['quantity'] }}</td>
                            </tr>
                            
                               
                            @endforeach
                        @endif
                       
                    </tbody>
                </table>

                <div class="subtotal">
                    <span class="text">Total</span>
                    <span class="price">&dollar;{{$subtotal ?? '' }}</span>
                </div>
                <div class="buttons">
                    <input type="submit" value="Update" name="update">
                    <input type="submit" value="PlaceOrder" name="placeorder">
                </div>
            </form>
        </div>
        </main>
        <footer id="burgerfooter">
        <div class="bgcolor">
            <img src="/images/5.png" class="logoFooter" align="center">
            <p> 
                <span id="title">Habla a:</span><br>
                Av. Intercomunal, sectro la Mora, calle 8
            </p>
            <p>
                <span id="title">Telefono:</span><br>
                +58 251 261 00 01
            </p>
            <p>
                <span id="title">Correo:</span><br>
                yourmail@gmail.com
            </p>
            <p>
                <a href="#" class="fa fa-pinterest"></a>
                <a href="#" class="fa fa-facebook"></a>
                <a href="#" class="fa fa-twitter"></a>
                <a href="#" class="fa fa-dribbble"></a>
                <a href="#" class="fa fa-google"></a>
                <a href="#" class="fa fa-linkedin"></a>
                <a href="#" class="fa fa-vimeo"></a>
            </p>
            <p>
                Copyright  &copy;2020 Todos los derechos reservados | Este sitio esta hecho con &hearts; por DiazApps
            </p>
        </div>
    </footer>
        // <script src="/js/script.js"></script>
    </body>
</html>