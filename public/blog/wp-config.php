<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'latestdb' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '0vb2ir|76^5n&X1@G,zj3AqGz5.51B665I=)W6:o[_<=0K0[*7kvzf]FWc;,3%E+' );
define( 'SECURE_AUTH_KEY',  '*G01+BVwXM.5EO(K>NDEP<@yfkhN7O.!^PK@hGk]{/z_;)wz9a)Y^8qwvgZ~<gq%' );
define( 'LOGGED_IN_KEY',    'ygc~>E~1pL2{p=SJUBH&Pvj|$7(nR*UvRjT?b6Mk7zg^vS5IKqi)meO.Kq~=zZSh' );
define( 'NONCE_KEY',        'U{0Gz^D #t@%Z?Z}pU39xCv%^a_p6-AQ{ s^Bh.j:?RplwO-u.??qxz$qPyn$q,N' );
define( 'AUTH_SALT',        'b|7o.iO16/Y*rBuW$l%TC1%(*|Ms:r(4[WK75U> weCL>H_69=!]vT^Fl:Iz`z6n' );
define( 'SECURE_AUTH_SALT', '=>HKEL47K~0ImQ,:/c=T0bB.56@7k)S]r6&$SgsfvIY{c; SmY^YL]IjfIP8Lrsx' );
define( 'LOGGED_IN_SALT',   'Hn`P{RDZ4h<|3kyvo.t^mg:t/5V]]oP3B+XK1I^O4e|n@E(RK4EnHZVd[T.EF3{A' );
define( 'NONCE_SALT',       '1`iZrh!h;@3E:l/<t:Tku]ao%-(n2x5jnA}u)p:kKKl#> ,u:Hx@`p?#Gw[q,u0H' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
