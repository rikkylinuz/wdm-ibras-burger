<h2>Ibras Burger</h2>
An food restaurant website where users can view menu, order food online, read recipes, etc. Admin manages the website by updating daily menu, blog posts, etc.

<h2>Built with</h2>
PHP<br>
Laravel<br>
Javascript<br>
MySQL<br>
HTML5<br>
CSS
